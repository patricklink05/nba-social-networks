import requests
from bs4 import BeautifulSoup, Comment
import pandas as pd
import networkx as nx
import matplotlib.pyplot as plt
import operator
from sklearn.cluster import KMeans
import numpy as np
from scipy.stats import linregress
from scipy.stats import ttest_ind_from_stats


        #comments = str(comment)

class League(object):
    def __init__(self, teams, years, special_list=None):
        self.years = years
        self.teams = {}
        self.playerlist = {"All" : []}
        self.special_list = special_list
        for year in self.years:
            self.playerlist[year] = []
        if special_list:
            self.playerlist["Special"] = []
        for team in teams:
            self.teams[team] = Team(team, self.years, self)

    def build_point_list(self, year):
        players = self.playerlist[year]
        point_list = pd.DataFrame(columns=["Player", "PPG"])
        index = 0
        for player in players:
            row = [player.name, player.ppg[year]]
            point_list.loc[index] = row
            index += 1
        return point_list


    def to_edgelist(self, year):
        players = self.playerlist[year]
        edgelist = pd.DataFrame(columns=["Player1", "Player2"])
        size = len(edgelist)
        index = 0
        for player in players:
            print(index)
            for year1, team in player.season.items():
                for teammate in self.teams[team].rosters[year1]:
                    if teammate in players and teammate != player:
                        edge = [player.name, teammate.name]
                        #print("Edge: ", edge)
                        edgelist.loc[index] = edge
                        index += 1
        return edgelist



class Team(object):
    def __init__(self, team, years, League, stats=[("PTS/G","per_game"), ("PER", "advanced"),("Age", "per_game")]):
        self.years = years
        self.team = team
        self.League = League
        self.rosters = {}
        self.fill_roster(stats)


    def fill_roster(self, stats):
        advanced = ["PER", "TS%", "3PAr", "FTr", "ORB%", "DRB%", "TRB%", "AST%", "STL%", "BLK%", "TOV%", "USG%", "OWS", "DWS",
        "WS", "WS/48", "OBPM", "DBPM", "BPM", "VORP"]
        if isinstance(stats, tuple):
            stat = ""
            stat = stats[0]
            table = stats[1]
        else:
            stat = []
            table = []
            for edge in stats:
                stat.append(edge[0])
                table.append(edge[1])
        for year in self.years:
            print(self.team, year)
            if self.team == "BRK" and int(year) <= 2012:
                print("out")
                print(self.team, year)
                team = "NJN"
            elif self.team == "CHO" and 2006 <= int(year) <= 2014:
                team = "CHA"
            elif self.team == "CHO" and int(year) <= 2005:
                continue
            elif self.team == "MEM" and int(year) <= 2001:
                team = "VAN"
            elif self.team == "NOP" and int(year) == 2006 or int(year) == 2007:
                team = "NOK"
            elif self.team == "NOP" and 2003 <= int(year) <= 2013:
                team = "NOH"
            elif self.team == "NOP" and int(year) <= 2002:
                continue
            elif self.team == "OKC" and int(year) <= 2008:
                team = "SEA"
            else:
                team = self.team


            data = requests.get("https://www.basketball-reference.com/teams/{}/{}.html".format(team, year))
            soup = BeautifulSoup(data.content)
            #print(soup.find_all('table'))
            columns = []
            if isinstance(stat, list):
                for instance in stat:
                    columns.append(stat[0])
            else:
                columns.append(stat[0])
            dfs = self.get_stats(soup, self.team, table)
            for key, val in dfs.items():
                if not isinstance(val, pd.DataFrame):
                    continue
            #if not isinstance(df, pd.DataFrame):
            #    continue
            #table = soup.find_all('table', {"class": "sortable stats_table"})[0]

            #df = pd.read_html(str(table))[0]
            #print(type(df))
            #df.insert(1, "Team", self.team)

            #df = df[["Player", "MP", "PER"]]
            players = []
            for row in dfs["Basic"].iterrows():
                if row[1].Player == "Taurean Prince":
                    p = Player("Taurean Waller-Prince", self.League)
                elif row[1].Player[-4:] == "(TW)":
                    print(row[1].Player)
                    p = Player(row[1].Player[:-5], self.League)
                else:
                    p = Player(row[1].Player, self.League)
                try:
                    index = self.League.playerlist["All"].index(p)
                    p = self.League.playerlist["All"][index]
                except ValueError:
                    self.League.playerlist["All"].append(p)
                for stat in stats:
                    df = dfs[stat[1]]
                    #print(stat[0])
                    try:
                        val = df.loc[df["Player"] == p.name][stat[0]].values[0]
                        #print(val)
                        p.add_stat(year,stat[0],val)
                        #print("adding: ", stat[0], val)
                    except IndexError:
                        #print("passing", stat[0])
                        pass

                #p.add_ppg(year, row[1]["PER"])
                p.add_season(year, self.team)
                players.append(p)
                if self.League.special_list:
                    if p.name in self.League.special_list:
                        self.League.playerlist["Special"].append(p)
                self.League.playerlist[year].append(p)
            #print(self.team,": ", df)
            self.rosters[year] = players

    def get_stats(self, soup, team, table_choice="advanced"):
        comments = ""
        try:
            init_table = soup.find_all('table', {"class": "sortable stats_table"})[0]
        except IndexError:
            #print(soup)
            #print(team)
            init_table = soup.find_all('table', {"class": "sortable stats_table"})
            print(init_table, team)
        dfs = {}
        df = pd.read_html(str(init_table))[0]
        dfs["Basic"] = df
        for comment in soup.find_all(text=lambda text:isinstance(text, Comment)):
            #print("comment: ",comment)
            soup2 = BeautifulSoup(str(comment))
            #print(soup2)

            #try:

            if isinstance(table_choice, list):
                unique_choices = list(set(table_choice))
                for i,table_ in enumerate(unique_choices):
                    table = soup2.find_all('table', {'id':table_})
                    try:
                        df = pd.read_html(str(table))[0]
                        df.rename(columns={'Unnamed: 1': "Player"}, inplace=True)
                        dfs[table_] = df
                    except ValueError:
                        pass
            else:
                #print(type(table_choice))
                #print(soup2.find_all('table'))
                table = soup2.find_all('table', {'id':table_choice})
                try:
                    df = pd.read_html(str(table))[0]
                    df.rename(columns={'Unnamed: 1': "Player"}, inplace=True)
                    dfs[table_choice] = df
                except ValueError:
                    pass
                #print(df)

            #except ValueError:
            #    pass
        #print(dfs)
        return dfs



class Player(object):
    def __init__(self, name, League):
        self.name = name
        self.League = League
        self.season = {}
        self.stats = {}

    def __eq__(self, other):
        return self.name == other.name

    def add_season(self, year, team):
        self.season[year] = team

    def add_stat(self, year, stat, value):
        try:
            self.stats[year][stat] = value
        except KeyError:
            self.stats[year] = {}
            self.stats[year][stat] = value
        #print(self.stats[year])

def f(row):
    if row["Player"] in executives and row["Player"] in player_reps:
        val = 5
    elif row["Player"] in executives and row["Player"] in alternates:
        val = 5
    elif row["Player"] in executives:
        val = 4
    elif row["Player"] in player_reps:
        val = 3
    elif row["Player"] in alternates:
        val = 2
    else:
        val = 1
    return val


if __name__ == "__main__":
    #fig = plt.figure()
    #comps = {"Eigenvector" : pd.read_csv("~/nbastuff/PER_vs_eigenvectorcentrality.csv"),
    #         "Degree" : pd.read_csv("~/nbastuff/PER_vs_degreecentrality.csv"),
    #         "Betweenness" : pd.read_csv("~/nbastuff/PER_vs_betweennesscentrality.csv"),
    #         "Closeness" : pd.read_csv("~/nbastuff/PER_vs_closenesscentrality.csv")
    #}
    executives = ["Chris Paul", "Andre Iguodala", "Anthony Tolliver",
                  "Bismack Biyombo", "Malcolm Brogdon", "Jaylen Brown",
                  "Pau Gasol", "C.J. McCollum", "Garrett Temple"]
    player_reps = ["Kent Bazemore", "Daniel Theis", "Rondae Hollis-Jefferson",
                   "Frank Kaminsky", "Zach LaVine", "J.R. Smith", "Devin Harris",
                   "Mason Plumlee", "Blake Griffin", "Jordan Bell", "P.J. Tucker",
                   "Myles Turner", "Danilo Gallinari", "Kyle Kuzma", "Ivan Rabb",
                   "Kelly Olynyk", "Donte DiVincenzo", "Karl-Anthony Towns",
                   "Solomon Hill", "Lance Thomas", "Russell Westbrook",
                   "Aaron Gordon", "Furkan Korkmaz", "Kelly Oubre",
                   "Damian Lillard", "Harry Giles", "Derrick White", "Kyle Lowry",
                   "Thabo Sefolosha", "Bradley Beal"]
    alternates = ["Taurean Prince", "Jaylen Brown", "Jared Dudley",
                  "Devonte Graham", "Wendell Carter Jr. ", "Cedi Osman",
                  "Dwight Powell", "Isaiah Thomas", "Jacob Evans III",
                  "Austin Rivers", "Alize Johnson", "Josh Hart",
                  "Jaren Jackson Jr.", "Josh Richardson", "Josh Okogie",
                  "Tim Frazier", "Terrance Ferguson", "Jonathan Isaac",
                  "Mikal Bridges", "Maurice Harkless", "Marvin Bagley",
                  "DeMar DeRozan", "Donovan Mitchell", "Sam Dekker"]

    all = executives + player_reps + alternates
    print(all)
    subplots = {"Eigenvector" : 221,
                "Degree" : 222,
                "Betweenness" : 223,
                "Closeness" : 224}
    #for key, data in comps.items():
    """

        data["Status"] = data.apply(f, axis=1)

        #std = data.loc["Status"][1].std()
        #print(mean, std)
        #print(data.groupby(['Status']).mean().Centrality,
        #      data.groupby(['Status']).std().Centrality)
        mean = data.groupby(['Status']).mean().Centrality
        std_dev = data.groupby(['Status']).std().Centrality
        count = data.groupby(["Status"]).count().Centrality
        errorbars = pd.DataFrame({
        "mean" : mean,
        "std_dev" : std_dev,
        "count" : count
        })
        print(errorbars)

        errorbars["error"] = errorbars.apply(lambda row: 1.96 * (float(row["std_dev"]) / np.sqrt(float(row["count"]))), axis=1)
        print(errorbars)
        subplot = subplots[key]
        #slope, intercept, r_value, p_value, std_err = linregress(data["Status"], data["Centrality"])
        #x = [label * slope for label in data["Status"]]
        ax1 = fig.add_subplot(subplot)
        #print(data["Status"],
        #    data.groupby(['Status']).mean().Centrality)
        xticks = []
        for x in range(1,6):
            tstat, pvalue = ttest_ind_from_stats(data["Centrality"].mean(),
                                                 data["Centrality"].std(),
                                                 data["Centrality"].count(),
                                                 errorbars.loc[x]["mean"],
                                                 errorbars.loc[x]["std_dev"],
                                                 errorbars.loc[x]["count"])
            print("pvalue: ", pvalue)
            sting = str(x) + ": " + str(round(pvalue,2))
            xticks.append(sting)
        ax1.bar(data.groupby(["Status"]).mean().index, data.groupby(['Status']).mean().Centrality, yerr=errorbars["error"])
        ax1.plot([1,5], [data["Centrality"].mean(),data["Centrality"].mean()], label=str(data["Centrality"].mean()))
        ax1.text(3, 1.1 * data["Centrality"].mean(), str(round(data["Centrality"].mean(),2)))
        print(xticks)
        plt.xticks(range(1,6),xticks, rotation=30)
        #plt.setp(xticks, rotation=45)
        #ax1.legend(loc='best')
        ax1.set_ylabel('Centrality')
        ax1.set_title(key, fontsize=10)
    fig.align_labels()
    plt.suptitle("Status vs. Centrality:", fontsize=18)
    plt.show()


    """

    teams = ["ATL","BRK","BOS", "CHO", "CHI", "CLE", "DAL", "DEN", "DET", "GSW",
             "HOU", "IND", "LAC", "LAL"	, "MEM", "MIA", "MIL", "MIN", "NOP", "NYK",
             "OKC", "ORL", "PHI", "PHO", "POR", "SAC", "SAS", "TOR", "UTA", "WAS"]
    #teams =["ATL", "PHI", "WAS"]
    #years = ["2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008",
    #         "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017",
    #         "2018", "2019"]
    years = ["2019"]
    #print(years[9:])
    #L = League(teams, years, special_list=all)
    #age_dict = {}
    #for player in L.playerlist["2019"]:
    #    print(player.name, player.stats)
    #    try:
    #        age_dict[player.name] = player.stats["2019"]["Age"]
    #    except KeyError:
    #        age_dict[player.name] = "null"
    #print(age_dict)
    #edgelist = L.to_edgelist("Special")
    #for year in years[9:]:
        #edgelist = L.to_edgelist(year)
        #edgelist.to_csv("{}edgelist.csv".format(year), index=False)
    #edgelist.to_csv("edgelistSpecial2019.csv", index=False)
    edgelist = pd.read_csv("~/nbastuff/2019edgelist.csv")
    #df.loc[~df.index.isin(t)]
    #print(edgelist)
    edgelist = edgelist.loc[~edgelist["Player1"].isin(all)]
    edgelist = edgelist.loc[~edgelist["Player2"].isin(all)]
    edgelist = edgelist.groupby(edgelist.columns.tolist(),as_index=False).size()
    edgelist = pd.DataFrame(edgelist)
    edgelist.reset_index(inplace=True)
    edgelist.rename(columns={0:'weight'}, inplace=True)
    G = nx.from_pandas_edgelist(edgelist, "Player1", "Player2", edge_attr="weight")
    betweenness = nx.eigenvector_centrality(G)
    #print(betweenness)
    New = sorted(betweenness, key=betweenness.get, reverse=True)
    #print(New)
    """
    with open("2019eigenvector.csv", 'w') as f:
        f.write("player,centrality\n")

        for item in New:
            print(item)
            try:
                print(str(item) + "," + str(betweenness[item]) + "," + str(age_dict[str(item)]))
                f.write(str(item) + "," + str(betweenness[item]) + "," + str(age_dict[str(item)]) + "\n")
            except KeyError:
                try:
                    print(str(item) + "," + str(betweenness[item]) + "," + str(age_dict[str(item[:-5])]))
                    f.write(str(item) + "," + str(betweenness[item]) + "," + str(age_dict[str(item[:-5])]) + "\n")
                except KeyError:
                    pass
    """
    print("Clustering Coefficient: ",nx.average_clustering(G))
    print("Nodes: ", len(G))
    print("Edges: ", G.number_of_edges())
    #ppg_vs_centrality = pd.DataFrame(columns=["Player", "Centrality", "PPG"])
    #index = 0
    #for row in ppglist.iterrows():
    #    edge = [row[1].Player, betweenness[row[1].Player], row[1].PPG]
    #    ppg_vs_centrality.loc[index] = edge
    #    index += 1
    #ppg_vs_centrality.to_csv("PER_vs_closenesscentrality.csv", index=False)
    e_1 = [(u, v) for (u, v, d) in G.edges(data=True) if d['weight'] == 1]
    e_2 = [(u, v) for (u, v, d) in G.edges(data=True) if d['weight'] == 2]
    e_3 = [(u, v) for (u, v, d) in G.edges(data=True) if d['weight'] == 3]
    e_4 = [(u, v) for (u, v, d) in G.edges(data=True) if d['weight'] == 4]
    e_5 = [(u, v) for (u, v, d) in G.edges(data=True) if d['weight'] == 5]
    e_6 = [(u, v) for (u, v, d) in G.edges(data=True) if d['weight'] == 6]
    e_7 = [(u, v) for (u, v, d) in G.edges(data=True) if d['weight'] == 7]
    e_8 = [(u, v) for (u, v, d) in G.edges(data=True) if d['weight'] == 8]
    e_9 = [(u, v) for (u, v, d) in G.edges(data=True) if d['weight'] >= 9]
    pos=nx.spring_layout(G)
    #labels = nx.get_edge_attributes(G,'weight')
    #print(labels[('Aaron Gordon', 'Evan Fournier')])
    nx.draw_networkx_edges(G, pos, edgelist=e_1, width=1.5, alpha=.4, edge_color='r')
    nx.draw_networkx_edges(G, pos, edgelist=e_2, width=2, alpha=.4, edge_color='r')
    nx.draw_networkx_edges(G, pos, edgelist=e_3, width=2.5, alpha=.4, edge_color='r')
    nx.draw_networkx_edges(G, pos, edgelist=e_4, width=1.5, alpha=.4, edge_color='b')
    nx.draw_networkx_edges(G, pos, edgelist=e_5, width=2, alpha=.4, edge_color='b')
    nx.draw_networkx_edges(G, pos, edgelist=e_6, width=2.5, alpha=.4, edge_color='b')
    nx.draw_networkx_edges(G, pos, edgelist=e_7, width=1.5, alpha=.4, edge_color='g')
    nx.draw_networkx_edges(G, pos, edgelist=e_8, width=2, alpha=.4, edge_color='g')
    nx.draw_networkx_edges(G, pos, edgelist=e_9, width=2.5, alpha=.4, edge_color='g')
    nx.draw(G, pos, with_labels=True)

    plt.show()
    #print(L.playerlist["Special"])
    #for player in L.teams["PHI"].rosters["2019"]:
    #    print(player.name, player.stats)
    #    if player.name == "Jimmy Butler":
    #        print(player.name,"'s 2018 PER: ", player.stats["2019"]["PER"])
        #for key, val in player.stats["2019"].items():
        #    print(val)
            #print(type(key), type(val))

    tables = ["advanced", "per_poss", "per_minute", "totals", "per_game"]
    stats = ["Age", "G", "GS", "MP", "FG", "FGA", "FG%", "3P", "3PA", "3P%", "2P", "2PA", "2P%", "eFG%",
             "FT", "FTA", "FT%", "ORB", "DRB", "TRB", "AST", "STL", "BLK", "TOV", "PF", "PTS/G", "PTS", "ORtg", "DRtg",
             "PER", "TS%", "3PAr", "FTr", "ORB%", "DRB%", "TRB%", "AST%", "STL%", "BLK%", "TOV%", "USG%", "OWS", "DWS",
             "WS", "WS/48", "OBPM", "DBPM", "BPM", "VORP"]
