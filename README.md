# NBA Social Networks Project
## Professor David Hachen, University of Notre Dame
## SOC 43990, Spring 2019

Sites used: https://www.basketball-reference.com/
            https://nbpa.com/leadership/ 

Packages used: bs4
	       pandas
	       numpy
	       scipy
	       networkx

This project seeks to find a connection between network centrality measures and NBPA leadership positions. 
brefscrape and scraper are very similar, I used brefscrape until I decided to refactor and clean things up in a separate file. Scraper is the most up to date model.  
