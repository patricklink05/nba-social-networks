import requests
from bs4 import BeautifulSoup, Comment
import pandas as pd
import networkx as nx
import matplotlib.pyplot as plt
import operator


def build_edge_list(teams, year):
    edgelist = pd.DataFrame(columns=["player", "team"])
    index = 0

    for team in teams:
        data = requests.get("https://www.basketball-reference.com/teams/{}/{}.html".format(team, year))
        soup = BeautifulSoup(data.content)
        #print(soup.find_all('table'))

        get_stats(soup, team)
        table = soup.find_all('table', {"class": "sortable stats_table"})[0]

        df = pd.read_html(str(table))[0]
        print(type(df))
        df.insert(1, "Team", team)
        print(df)
        size = len(df)
        for i in range(size):
            #print(type(df.MP[i]))
            edge = [df.Player[i], df.Team[i]]
            edgelist.loc[index] = edge
            index += 1
            break
            #for j in range(size):
                #if i < j:
                    #edge = [df.Player[i], df.Player[j]]
                    #edgelist.loc[index] = edge
                    #index += 1
                    #break
                    #print(edge)

    return edgelist

def get_stats(soup, team):


    comments = ""
    for comment in soup.find_all(text=lambda text:isinstance(text, Comment)):
        #print("comment: ",comment)
        soup2 = BeautifulSoup(str(comment))
        #print(soup2)

        try:
            table = soup2.find_all('table', {'id':'advanced'})
            df = pd.read_html(str(table))[0]
            #print(df)
            return df
        except ValueError:
            pass

        #comments = str(comment)

def build_2mode(teams, year):
    edgelist = pd.DataFrame(columns=["player", "team", "minutes", "points"])
    index = 0

    for team in teams:
        data = requests.get("https://www.basketball-reference.com/teams/{}/{}.html".format(team, year))
        soup = BeautifulSoup(data.content)
        #print(soup.find_all('table'))
        df = get_stats(soup, team)
        if not isinstance(df, pd.DataFrame):
            continue
        #table = soup.find_all('table', {"class": "sortable stats_table"})[0]

        #df = pd.read_html(str(table))[0]
        #print(type(df))
        df.insert(1, "Team", team)
        df.rename(columns={'Unnamed: 1': "Player"}, inplace=True)
        print(df)
        size = len(df)
        for i in range(size):
            if df.MP[i] > 0.0:
                edge = [df.Player[i], df.Team[i], df.MP[i], df["PTS/G"][i]]
                #edge = [df.Player[i], df.Team[i]]
                edgelist.loc[index] = edge
                index += 1
            #for j in range(size):
                #if i < j:
                    #edge = [df.Player[i], df.Player[j]]
                    #edgelist.loc[index] = edge
                    #index += 1
                    #break
                    #print(edge)

    return edgelist



class League(object):
    def __init__(self, teams, years):
        self.years = years
        self.teams = {}
        self.playerlist = {"All" : []}
        for year in self.years:
            self.playerlist[year] = []
        for team in teams:
            self.teams[team] = Team(team, self.years, self)

    def build_point_list(self, year):
        players = self.playerlist[year]
        point_list = pd.DataFrame(columns=["Player", "PPG"])
        index = 0
        for player in players:
            row = [player.name, player.ppg[year]]
            point_list.loc[index] = row
            index += 1
        return point_list


    def to_edgelist(self, year):
        players = self.playerlist[year]
        edgelist = pd.DataFrame(columns=["Player1", "Player2"])
        size = len(edgelist)
        index = 0
        for player in players:
            print(index)
            for year1, team in player.season.items():
                for teammate in self.teams[team].rosters[year1]:
                    if teammate in players and teammate != player:
                        edge = [player.name, teammate.name]
                        print("Edge: ", edge)
                        edgelist.loc[index] = edge
                        index += 1
        return edgelist



class Team(object):
    def __init__(self, team, years, League):
        self.years = years
        self.team = team
        self.League = League
        self.rosters = {}
        self.fill_roster()


    def fill_roster(self):
        for year in self.years:
            data = requests.get("https://www.basketball-reference.com/teams/{}/{}.html".format(self.team, year))
            soup = BeautifulSoup(data.content)
            #print(soup.find_all('table'))
            df = get_stats(soup, self.team)
            if not isinstance(df, pd.DataFrame):
                continue
            #table = soup.find_all('table', {"class": "sortable stats_table"})[0]

            #df = pd.read_html(str(table))[0]
            #print(type(df))
            #df.insert(1, "Team", self.team)
            df.rename(columns={'Unnamed: 1': "Player"}, inplace=True)
            df = df[["Player", "MP", "PER"]]
            players = []
            for row in df.iterrows():
                p = Player(row[1].Player, self.League)
                try:
                    index = self.League.playerlist["All"].index(p)
                    p = self.League.playerlist["All"][index]
                except ValueError:
                    self.League.playerlist["All"].append(p)
                p.add_ppg(year, row[1]["PER"])
                p.add_season(year, self.team)
                players.append(p)
                self.League.playerlist[year].append(p)
            #print(self.team,": ", df)
            self.rosters[year] = players


class Player(object):
    def __init__(self, name, League):
        self.name = name
        self.League = League
        self.season = {}
        self.ppg = {}

    def __eq__(self, other):
        return self.name == other.name

    def add_season(self, year, team):
        self.season[year] = team

    def add_ppg(self, year, ppg):
        self.ppg[year] = ppg

if __name__ == "__main__":
    """
    data = requests.get("https://www.basketball-reference.com/teams/PHI/2019.html")
    soup = BeautifulSoup(data.content)
    table = soup.find_all('table')[0]
    df = pd.read_html(str(table))
    size = len(df[0])
    edgelist = pd.DataFrame(columns=["node1", "node2"])
    index = 0
    for i in range(size):
        for j in range(size):
            if i < j:
                edge = [df[0].Player[i], df[0].Player[j]]
                edgelist.loc[index] = edge
                index += 1
                print(edge)
                """

    #edgedata = [df[0].Player[0], df[0].Player[1]]


    #edgelist.loc[1] = [df[0].Player[0], df[0].Player[2]]
    #with open("edgelistphi2019.csv", "w") as fil:
    #    fil.write(edge)
    teams = ["ATL","BRK","BOS", "CHO", "CHI", "CLE", "DAL", "DEN", "DET", "GSW",
             "HOU", "IND", "LAC", "LAL"	, "MEM", "MIA", "MIL", "MIN", "NOP", "NYK",
             "OKC", "ORL", "PHI", "PHO", "POR", "SAC", "SAS", "TOR", "UTA", "WAS"]
    #years = ["2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008",
    #         "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017",
    #         "2018", "2019"]
    years = ["2018", "2019"]
    #teams = ["BOS", "CHO", "CHI", "CLE", "DAL", "DEN", "DET", "GSW"]
    #L = League(teams, years)
    #edgelist = L.to_edgelist("2019")
    #edgelist.to_csv("edgelist2019.csv", index=False)
    #L.build_point_list("2019").to_csv("PER2019.csv", index=False)
    edgelist = pd.read_csv("~/nbastuff/edgelist2019.csv")
    ppglist = pd.read_csv("~/nbastuff/ppg2019.csv")
    edgelist = edgelist.groupby(edgelist.columns.tolist(),as_index=False).size()
    edgelist = pd.DataFrame(edgelist)
    edgelist.reset_index(inplace=True)
    edgelist.rename(columns={0:'weight'}, inplace=True)
    print(edgelist)
    print(ppglist)
    #print(edgelist)
    G = nx.from_pandas_edgelist(edgelist, "Player1", "Player2", edge_attr="weight")
    #newBetweenness = dict(sorted(nx.betweenness_centrality(G).iteritems(), key=operator.itemgetter(1), reverse=True)[:5])
    betweenness = nx.closeness_centrality(G)
    New = sorted(betweenness, key=betweenness.get, reverse=True)

    ppg_vs_centrality = pd.DataFrame(columns=["Player", "Centrality", "PPG"])
    index = 0
    for row in ppglist.iterrows():
        edge = [row[1].Player, betweenness[row[1].Player], row[1].PPG]
        ppg_vs_centrality.loc[index] = edge
        index += 1
    ppg_vs_centrality.to_csv("PER_vs_closenesscentrality.csv", index=False)



        #print(i, " ", player, ": ", betweenness[player])

    #print(L.build_point_list("2019"))
    #print(max(.items(), key=operator.itemgetter(1))[0],
    #max(nx.betweenness_centrality(G).items(), key=operator.itemgetter(1))[1])
    e_1 = [(u, v) for (u, v, d) in G.edges(data=True) if d['weight'] == 1]
    e_2 = [(u, v) for (u, v, d) in G.edges(data=True) if d['weight'] == 2]
    e_3 = [(u, v) for (u, v, d) in G.edges(data=True) if d['weight'] == 3]
    e_4 = [(u, v) for (u, v, d) in G.edges(data=True) if d['weight'] == 4]
    e_5 = [(u, v) for (u, v, d) in G.edges(data=True) if d['weight'] == 5]
    e_6 = [(u, v) for (u, v, d) in G.edges(data=True) if d['weight'] == 6]
    e_7 = [(u, v) for (u, v, d) in G.edges(data=True) if d['weight'] == 7]
    e_8 = [(u, v) for (u, v, d) in G.edges(data=True) if d['weight'] == 8]
    e_9 = [(u, v) for (u, v, d) in G.edges(data=True) if d['weight'] >= 9]
    pos=nx.spring_layout(G,weight="weight")
    #labels = nx.get_edge_attributes(G,'weight')
    #print(labels[('Aaron Gordon', 'Evan Fournier')])
    nx.draw_networkx_edges(G, pos, edgelist=e_1, width=1)
    nx.draw_networkx_edges(G, pos, edgelist=e_2, width=2)
    nx.draw_networkx_edges(G, pos, edgelist=e_3, width=3)
    nx.draw_networkx_edges(G, pos, edgelist=e_4, width=4)
    nx.draw_networkx_edges(G, pos, edgelist=e_5, width=5)
    nx.draw_networkx_edges(G, pos, edgelist=e_6, width=6)
    nx.draw_networkx_edges(G, pos, edgelist=e_7, width=7)
    nx.draw_networkx_edges(G, pos, edgelist=e_8, width=8)
    nx.draw_networkx_edges(G, pos, edgelist=e_9, width=9)
    nx.draw(G, pos, with_labels=True)

    plt.show()

    #i = 40
    #for player in L.playerlist["All"]:
    #    print("Player: ", player.name, " ", player.season)
    #print(L.teams["PHI"].rosters["2019"])

    #edgelist = build_2mode(teams,"2019")
    #G = nx.from_pandas_edgelist(edgelist, "player", "team")
    #nx.draw(G)
    #plt.show()

    #edgelist.to_csv("edgelistphi2019.csv")
    #print(edgelist)
    #print(soup.table.get_text())
    #print(data.content)
